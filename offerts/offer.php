<?php
 session_start();
 if((!isset($_SESSION['logged'])) && ($_SESSION['logged']==false))
 {
   header('Location: ../index.php');
   exit();
 }

 if(isset($_POST['prize']))
 {
   $is_ok = true;
   $offertname= $_POST['offertname'];
   if(strlen($offertname)<3 || (strlen($offertname)>30))
   {
     $is_ok = false;
     $_SESSION['e_offertname']= "Nazwa oferty musi posiadać od 3 do 30 znaków!";
   }
   $content = $_POST['content'];
   if(strlen($content)<20 || (strlen($content)>1000))
   {
     $is_ok=false;
     $_SESSION['e_content'] = "Opis oferty musi posiadać od 20 od 1000 znaków!";
   }
   $prize= $_POST['prize'];
   $term='/^[1-9]{1,1}+[0-9]{0,10}+(\.|\,){1}+[0-9]{2,2}+$/';
   if(!(preg_match($term,$prize)))
   {
     $is_ok=false;
     $_SESSION['e_prize']= "Cena jest niepoprawna!";
   }
   $amount= $_POST['amount'];

   if($amount<=0) {
     $is_ok=false;
     $_SESSION['e_amount']= "Wprowadzono nieprawidłową ilość!";
   }
   require_once "../connect.php";
   mysqli_report(MYSQLI_REPORT_STRICT);

   try {
     $connection = new PDO('mysql:host='.$host.';dbname='.$db_name.';port='.$port, $db_user, $db_password );
      if (mysqli_connect_errno() != 0) {
          throw new exception(mysqli_connect_errno());
        }
      else {
         if($is_ok==true)
         {
           if($connection->query("INSERT INTO offert VALUES (NULL,'$offertname', '$content', '$prize','$amount')")){
             $_SESSION['successfuladd']=true;
             header('Location: added.php');
           }
           else {
             throw new exception($connection->error);
           }
           exit();
           }
           //$connection->mysql_close();
         }
      }
   catch (PDOException $e) {
       echo '<div class="Error">Błąd serwera!</div>';
       echo '<br />Informacja developerska: '.$e->GetMessage();
       die();
     }
   }

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8" />
<link rel="stylesheet" href="css/style.css" />
</head>
<body>
<form method="post">
  <label for="offertname">Nazwa oferty:</label> <br />
   <input type="text" name="offertname"><br /><br />
   <?php
       if(isset($_SESSION['e_offertname']))
       {
         echo '<div class="error">'. $_SESSION['e_offertname']. '</div>'.'<br />'.'<br />';
         unset($_SESSION['e_offertname']);
       }
    ?>
   <label for="content" id="content" >Opis:</label><br />
   <textarea rows="10" cols="50"   name="content" ></textarea><br /><br />
   <?php
       if(isset($_SESSION['e_content']))
       {
         echo '<div class="error">'. $_SESSION['e_content']. '</div>'.'<br />'.'<br />';
         unset($_SESSION['e_content']);
       }
    ?>

   <label for="prize" id="prize">Cena:</label><br />
   <input type="text" name="prize"><br /><br />
   <?php
       if(isset($_SESSION['e_prize']))
       {
         echo '<div class="error">'. $_SESSION['e_prize']. '</div>'.'<br />'.'<br />';
         unset($_SESSION['e_prize']);
       }
    ?>
   <label for="amount">Ilość:</label><br />
   <input type="number" name="amount"><br /><br />
   <?php
       if(isset($_SESSION['e_amount']))
       {
         echo '<div class="error">'. $_SESSION['e_amount']. '</div>'.'<br />'.'<br />';
         unset($_SESSION['e_amount']);
       }
    ?>
   <input type="submit" value="Wystaw ofertę">

</form>

<br /> <br /> <br />
<a href="../user.php">Powrót</a>
</body>
</html>
