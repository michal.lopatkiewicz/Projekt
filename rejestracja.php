<?php
 session_start();
 if(isset($_POST['email']))
 {
   $is_ok = true;  //walidacja OK
   $nick = $_POST['nick'];
   if ((strlen($nick)<3) || (strlen($nick)>20))
   {
     $is_ok = false;                //sprawdzanie długości nicku
     $_SESSION['e_nick'] = "Nazwa użytkownika musi posiadać od 3 do 20 znaków!";
   }
   if (ctype_alnum($nick)==false)
   {
     $is_ok=false;            //sprawdzanie
     $_SESSION['e_nick'] = "Nazwa użytkownika może składać się tylko z liter i cyfr (bez polskich znaków)!";
   }
   $email= $_POST['email'];
   $emailB = filter_var($email, FILTER_SANITIZE_EMAIL); //usuwanie nieprawidłowych znakow z email
   if((filter_var($emailB, FILTER_VALIDATE_EMAIL)==false) || ($emailB!=$email)) // sprawdzanie poprawnosci email
   {
     $is_ok = false;
     $_SESSION['e_email'] = "Niepoprawny adres e-mail!";
   }

  $name = $_POST['name'];
  if(strlen($name>20))
  {
    $is_ok = false;
    $_SESSION['e_name'] = "Zbyt długie imię!";
    }
  $name =ucfirst($name);   //Ustawia pierwszą litere jako duza
  $term= '/^[A-ZŁŚ]{1}+[a-ząęółśżźćń]+$/';

  if (!(preg_match($term,$name)))
  {
    $is_ok=false;            //sprawdzanie
    $_SESSION['e_name'] = "Błędne imię!";

  }

  $forname = $_POST['forname'];

  if(strlen($forname>25))
  {
    $is_ok = false;
    $_SESSION['e_forname'] = "Zbyt długie nazwisko!";
    }
  $forname =ucfirst($forname);   //Ustawia pierwszą litere jako duza

  if (!(preg_match($term,$forname)))
  {
    $is_ok=false;            //sprawdzanie
    $_SESSION['e_forname'] = "Błędne nazwisko!";
  }

   $password1=$_POST['password1'];
   $password2=$_POST['password2'];

   if((strlen($password1)<8) || (strlen($password1) >20)) // sprawdzanie długości hasła
   {
     $is_ok = false;
     $_SESSION['e_password'] = "Hasło musi posiadać od 8 do 20 znaków!";
   }
   if($password1!=$password2)     //sprawdzanie czy hasła są takie same
   {
     $is_ok= false;
     $_SESSION['e_password'] = "Podane hasła nie są takie same!";
   }

   $password_hash = password_hash($password1, PASSWORD_DEFAULT);  ///hashowanie hasła
   //exit();

   if (!isset($_POST['rules']))     // sprawdzenie czy checkbox jest kliknięty
   {
     $is_ok = false;
     $_SESSION['e_rules']= "Potwierdź regulamin!";
   }

   $secret = "6LeCzTUUAAAAAOZUDKovmTuGCvGH_SjekfeXPwey";

   $check = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
   $response= json_decode($check);

   if(!($response->success))
   {
     $is_ok = false;
     $_SESSION['e_captcha']= "Potwierdź, że nie jesteś botem!";
   }

   require_once "connect.php";
   mysqli_report(MYSQLI_REPORT_STRICT);     //wrzucanie ostrzeżen do wyjatkow połączenia z baza
   try
   {
     $connection = new mysqli($host, $db_user, $db_password, $db_name);
     if($connection->connect_errno!=0)
     {
       throw new Exception(mysqli_connect_errno());
     }
     else
     {    // sprawdzanie czy email jest zajęty
          $result = $connection->query ("SELECT id FROM uzytkownicy WHERE email='$email'");
          if (!$result)
          {
            throw new exception($connection->error);
          }
          $number_such_emails = $result->num_rows;
          if ($number_such_emails>0)
            {
              $is_ok = false;
              $_SESSION['e_email']= "Istnieje już konto z takim adresem email!";

            }

            // sprawdzanie czy nick jest zajęty
            $result = $connection->query ("SELECT id FROM uzytkownicy WHERE user='$nick'");
            if (!$result)
            {
              throw new exception($connection->error);
            }
            $number_such_users = $result->num_rows;
            if ($number_such_users>0)
              {
                $is_ok = false;
                $_SESSION['e_nick']= "Istnieje już konto z takim loginem!";
              }
    if($is_ok==true)
    {
      if($connection->query("INSERT INTO uzytkownicy VALUES (NULL,'$name','$forname','$nick', '$password_hash', '$email')"))
      {
        $_SESSION['successfulregistration']=true;
        header('Location: welcome.php');
      }
      else {
      throw new exception($connection->error);
      }
      exit();
      }
    $connection->close();
     }
   }
   catch(Exception $e)
   {
     echo '<div class="Error">Błąd serwera!</div>';
     echo '<br />Informacja developerska: '.$e;
   }
}


?>

<html>
<head>
  <meta charset="UTF-8" />
  <link rel="stylesheet" href="css/style.css" />
  <title>Załóż darmowe konto!</title>
  <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body>
<div class="form">
<form method="post">
  Imię: <br /> <input type="text" name="name" /> <br />
  <?php
      if(isset($_SESSION['e_name']))
      {
        echo '<div class="error">'. $_SESSION['e_name']. '</div>';
        unset($_SESSION['e_name']);
      }
   ?>
   Nazwisko: <br /> <input type="text" name="forname" /> <br />
   <?php
       if(isset($_SESSION['e_forname']))
       {
         echo '<div class="error">'. $_SESSION['e_forname']. '</div>';
         unset($_SESSION['e_forname']);
       }
    ?>

  Login: <br /> <input type="text" name="nick" /> <br />
  <?php
      if(isset($_SESSION['e_nick']))
      {
        echo '<div class="error">'. $_SESSION['e_nick']. '</div>';
        unset($_SESSION['e_nick']);
      }
   ?>
  E-mail: <br /> <input type="text" name="email" /> <br />
  <?php
      if(isset($_SESSION['e_email']))
      {
        echo '<div class="error">'. $_SESSION['e_email']. '</div>';
        unset($_SESSION['e_email']);
      }
   ?>
  Hasło: <br /> <input type="password" name="password1" /> <br />
  <?php
      if(isset($_SESSION['e_password']))
      {
        echo '<div class="error">'. $_SESSION['e_password']. '</div>';
        unset($_SESSION['e_password']);
      }
   ?>
  Potwierdź hasło: <br /> <input type="password" name="password2" /> <br />

  <label>
  <input type="checkbox" name="rules" /> Akceptuje regulamin
  </label>
  <?php
      if(isset($_SESSION['e_rules']))
      {
        echo '<div class="error">'.$_SESSION['e_rules'].'</div>';
        unset($_SESSION['e_rules']);
      }
   ?>

  <div class="g-recaptcha" data-sitekey="6LeCzTUUAAAAAM1pkiTpU3aCcJ9gHTZL9_uEsPve"></div>
  <?php
      if(isset($_SESSION['e_captcha']))
      {
        echo '<div class="error">'.$_SESSION['e_captcha'].'</div>';
        unset($_SESSION['e_captcha']);
      }
   ?>
<br />
  <input type="submit" value="zarejestruj się"/>
</form>
</div>
</body>
</html>
