
<?php
session_start();
if (!isset($_SESSION['successfulregistration']))
{
  header('Location: index.php');
  exit();
}
else {
  unset($_SESSION['successfulregistration']);
}
 ?>
 <!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8" />
  <link rel="stylesheet" href="css/style.css" />
</head>
<body>
  <p>Rejestracja dokonana pomyślnie!</p> <br />
  <a href="logowanie.php"> Możesz zalogować się na swoje konto!</a>

</body>
</html>
