<?php
session_start();
if ((!isset($_POST['login'])) || (!isset($_POST['password'])))
{
  header ('Location: logowanie.php');
  exit();

}
require_once "connect.php";
$connection = new mysqli($host, $db_user, $db_password, $db_name);
if($connection->connect_errno!=0)
{
  echo "Error: ".$connection->connect_errno;
}
else
{
    $login = $_POST['login'];
    $password = $_POST['password'];

    $login = htmlentities($login, ENT_QUOTES, "UTF-8");    // zabezpieczenie przeciwko SQL injection
    $password= htmlentities($password, ENT_QUOTES, "UTF-8");      // poprzez ustawienie encji

    if($result = $connection->query(
      sprintf("SELECT * FROM uzytkownicy WHERE user ='%s'",
      mysqli_real_escape_string($connection, $login))))   // wykrywa próby wpływania na zapytania
                                                            // operatorami -- lub ''
    {
      $ilu_userow = $result ->num_rows;
        if($ilu_userow>0)
        {
            $line = $result -> fetch_assoc();
            if(password_verify($password, $line['pass']))
            {
            $_SESSION['logged'] = true;
            $_SESSION['id']= $line['id'];
            $_SESSION['user'] = $line['user'];
            $_SESSION['name'] = $line['name'];
            $_SESSION['forname']=$line['forname'];
            unset($_SESSION['error']);
            $result->free();
            header('Location: user.php');
          }
          else
          {
            $_SESSION['error'] = 'Nieprawidlowy login lub hasło!';
            header('Location: logowanie.php');
          }

        } else
        {
         $_SESSION['error'] = 'Nieprawidlowy login lub hasło!';
          header('Location: logowanie.php');
        }
    }
    $connection->close();
}
?>
